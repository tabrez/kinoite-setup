#!/bin/bash

sudo apt-get update && sudo apt-get upgrade -y
sudo apt-get install -y build-essential software-properties-common apt-transport-https fonts-jetbrains-mono jq
sudo apt-get install -y unzip wget curl git tree vim tmux htop net-tools iproute2 lsb-release git-lfs locales
sudo apt-get install -y trash-cli xclip xsel ncdu ranger bat ripgrep fd-find fzf neofetch exa zoxide duf direnv
sudo apt-get install -y pipx gnupg nano bpytop gdu powertop tldr python-is-python3 python3-pip

## install asdf
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.13.1
. ${HOME}/.asdf/asdf.sh

## install chezmoi & setup dotfiles
asdf plugin add chezmoi
asdf install chezmoi 2.42.1
asdf global chezmoi 2.42.1
chezmoi init --apply tabrez
. ~/.bashrc

## install neovim
asdf plugin add neovim
asdf install neovim stable
asdf global neovim stable

## install nodejs
asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git
asdf install nodejs latest
asdf global nodejs latest

## install & initialise astrovim
[ -d ~/.config/astrovim ] || git clone --depth 1 https://github.com/AstroNvim/AstroNvim ~/.config/astrovim
[ -d ~/.config/astrovim ] && NVIM_APPNAME=astrovim nvim --headless -c 'quitall'
rm -rf ~/.config/astrovim/.git

## install & initialise lazyvim
## [ -d ~/.config/lazyvim ] || git clone --depth 1 https://github.com/LazyVim/starter ~/.config/lazyvim
## [ -d ~/.config/astrovim ] && NVIM_APPNAME=astrovim nvim --headless -c 'quitall'
rm -rf ~/.config/lazynvim/.git

## setup host-spawn
#wget https://github.com/1player/host-spawn/releases/download/1.5.0/host-spawn-x86_64 -O /usr/local/bin/host-spawn
#sudo chown tabrez:tabrez /usr/local/bin/host-spawn
#chmod ug+x /usr/local/bin/host-spawn
#ln -s /usr/local/bin/host-spawn /usr/local/bin/flatpak
#ln -s /usr/local/bin/host-spawn /usr/bin/rpm-ostree
#ln -s /usr/local/bin/host-spawn /usr/bin/ostree
#ln -s /usr/local/host-spawn /usr/bin/rpm

bash install_monaspace_font.sh
