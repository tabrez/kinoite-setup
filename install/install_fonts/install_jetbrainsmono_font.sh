#!/bin/bash

wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/JetBrainsMono.zip
unzip -d ~/.local/share/fonts/JetBrainsMono/ ./JetBrainsMono.zip 
rm -f JetBrainsMono.zip

