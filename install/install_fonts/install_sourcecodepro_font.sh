#!/bin/bash

wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/SourceCodePro.zip
unzip -d ~/.local/share/fonts/sourcecodepro SourceCodePro.zip
rm -f SourceCodePro.zip

