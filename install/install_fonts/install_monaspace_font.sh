#!/bin/bash

DEST="$HOME/.local/share/fonts/monaspace"
mkdir -p "$DEST"
git clone https://github.com/githubnext/monaspace.git
cd monaspace
rsync -Pah fonts/otf/* "$DEST"
rsync -Pah fonts/variable/* "$DEST"
fc-cache -f
cd .. && rm -rf monaspace

