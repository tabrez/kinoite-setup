#!/bin/bash

sudo apt-get install -y flatpak
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo --user
flatpak install flathub com.valvesoftware.Steam --user -y
flatpak install flathub net.lutris.Lutris --user -y
flatpak install flathub com.valvesoftware.Steam.CompatibilityTool.Proton-GE --user -y
sudo apt-get install -y steam-devices
