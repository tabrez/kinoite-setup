#!/bin/bash

sudo apt-get install -y flatpak

flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo --user
# flatpak install flathub org.mozilla.firefox --user -y
# flatpak install flathub org.mozilla.Thunderbird --user -y
flatpak install flathub org.telegram.desktop --user -y
flatpak install flathub com.discordapp.Discord --user -y
flatpak install flathub md.obsidian.Obsidian --user -y
flatpak install flathub com.logseq.Logseq --user -y
# flatpak install flathub com.github.tchx84.Flatseal --user -y
# flatpak install flathub io.github.flattool.Warehouse --user -y

