code --install-extension GitHub.github-vscode-theme
code --install-extension ms-toolsai.jupyter
code --install-extension vscode-icons-team.vscode-icons
code --install-extension VisualStudioExptTeam.vscodeintellicode
code --install-extension asvetliakov.vscode-neovim

code --install-extension ms-python.python
code --install-extension ms-python.vscode-pylance
code --install-extension charliermarsh.ruff

code --install-extension ms-vscode.remote-server
code --install-extension ms-vscode-remote.remote-containers
code --install-extension ms-vscode-remote.remote-ssh
code --install-extension ms-vscode-remote.remote-ssh-edit
