# Setup Ubuntu 23.10

Note: Most of the following is covered by `setup_ubuntu.sh` script. If using the script, only follow manual steps like
installing games and setting up smb credentials file.

## Update system

```bash
sudo apt update && sudo apt -y upgrade
sudo snap refresh
```

## Setup gaming

+ Run `bash setup_gaming.sh`
+ Run Lutris and install `Battle.net Launcher`, don't login at the end of the installation
+ Run `Battle.net Launcher`, login, and install `Diablo 4`
+ Run `Steam` and install other games like `Last Epoch`

## Setup shared folders

+ Create `~/smb_creds` with the following credentials:

```text
username=tabrez
password=so...
```

+ Run  the following commands:

```bash
mkdir ~/smb ~/nfs
sudo echo '//192.168.2.133/smb /home/tabrez/smb cifs credentials=/home/tabrez/smb_creds,guest,file_mode=0644,dir_mode=0755,uid=1000,gid=1000 0 0' >> /etc/fstab
sudo echo '192.168.2.133:/mnt/truenas/nfs /home/tabrez/nfs nfs defaults,timeo=800,retrans=5,_netdev 0 0'
sudo mount -a
```

## Copy Firefox profile from shared folders

+ Make a backup of current profile folder if you like:
`mv ~/snap/firefox/common/.mozilla/firefox ~/snap/firefox/common/.mozilla/firefox2`
+ Run `rsync -Pah ~/smb/mega/misc/profiles/Firefox` to `/home/tabrez/snap/firefox/common/.mozilla/firefox`
+ Run `firefox -P` and select `default-release` profile

## Copy ssh keys

```bash
cp ~/smb/mega/main/ssh_keys/id_rsa* ~/.ssh/
chmod 600 ~/.ssh/id_rsa
```

## Install docker

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo groupadd docker
sudo usermod -aG docker $USER
```

## Install Distrobox

```bash
wget -qO- https://raw.githubusercontent.com/89luca89/distrobox/main/install | sh -s -- --prefix ~/.local
export PATH=$PATH:/home/tabrez/.local/bin
cd /home/tabrez/smb/mega/devops/distrobox/main
distrobox assemble create
```

