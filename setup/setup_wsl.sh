wget https://mega.nz/linux/repo/xUbuntu_22.04/amd64/megacmd-xUbuntu_22.04_amd64.deb -O megacmd.deb
sudo apt-get install -y ./megacmd.deb
rm -f megacmd.deb

cat << 'EOF'
mega-login tabrez@mailbox.org 1V...
mega-ls
exclude -a .venv -a results
exclude -d .*
mega-sync mega/code/huggingface/ MEGA/code/huggingface/
mega-sync mega/devops/distrobox/pybox/ MEGA/devops/distrobox/pybox/
EOF
