#!/bin/bash

# wget https://mega.nz/linux/repo/Fedora_39/x86_64/megasync-Fedora_39.x86_64.rpm -O megasync.rpm
# rpm-ostree install megasync.rpm
# rm -rf megasync.rpm
#
wget --content-disposition https://mullvad.net/download/app/rpm/latest -O mullvad.rpm
rpm-ostree install mullvad.rpm
rm -f mullvad.rpm
sudo systemctl enable --now mullvad-daemon

# wget https://update.code.visualstudio.com/latest/linux-rpm-x64/stable -O vscode.rpm
# rpm-ostree install vscode.rpm
# rm -rf vscode.rpm
#
# rpm-ostree install jetbrains-mono-fonts
bash install_jetbrainsmono_font.sh

