#!/bin/bash

# update packages
sudo apt update && sudo apt -y upgrade
sudo snap refresh

# install gaming software
# bash setup_gaming.sh

# setup sharing
mkdir ~/smb
sudo echo '//192.168.2.133/smb /home/tabrez/smb cifs credentials=/home/tabrez/smb_creds,guest,file_mode=0644,dir_mode=0755,uid=1000,gid=1000 0 0' >>/etc/fstab
sudo echo '192.168.2.133:/mnt/truenas/nfs /home/tabrez/nfs nfs defaults,timeo=800,retrans=5,_netdev 0 0'
sudo mount -a
mv ~/snap/firefox/common/.mozilla/{firefox,firefox2}
rsync -Pah ~/smb/mega/misc/profiles/Firefox ~/snap/firefox/common/.mozilla/firefox
cp ~/smb/mega/main/ssh_keys/id_rsa* ~/.ssh/
chmod 600 ~/.ssh/id_rsa

# install docker using convenience script
# curl -fsSL https://get.docker.com -o get-docker.sh
# sudo sh get-docker.sh
# sudo groupadd docker
# sudo usermod -aG docker $USER
sudo apt-get install -y podman

# install distrobox
wget -qO- https://raw.githubusercontent.com/89luca89/distrobox/main/install | sh -s -- --prefix ~/.local
# export $PATH=$PATH:/home/tabrez/.local/bin
cd /home/tabrez/smb/mega/devops/distrobox/main
~/.local/bin/distrobox assemble create

# bash install_jetbrainsmono_font.sh
timedatectl set-local-rtc 1 --adjust-system-clock

