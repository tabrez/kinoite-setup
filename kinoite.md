# Setup Fedora Kinoite

## Install MEGAsync, Mullvad & JetBrains Mono fonts

```bash
wget https://gist.githubusercontent.com/tabrez/e70db2f45d2bb7b7ac4ece9f5997e423/raw/ -O setup_fedora.sh
bash setup_fedora.sh
```

## Configure desktop

+ Configure display settings
* Power button does sleep, no automatic sleep, monitor off after 7 minutes
+ Add keyboard shortcut to put system to sleep
+ Create new terminal profile and select jetbrains mono or github monaspace font
+ Select 'Large Icons' in Window Behaviour
+ Increase the size of task bar and make it auto-hide, configure pinned apps

## Sync folders using megasync

Note: better use nfs mount for the following folders instead of megasync

Login into megasync and sync the folders as needed.
Remove . from sync excludes, add .venv, data, dbs, mlruns to sync excludes

Add sync for the following folders:
+ mega/devops/distrobox/pybox
+ mega/code
+ mega/misc/ssh_keys

move private key to ~/.ssh/

## Configure fedora

```bash
bash setup_fedora.sh
```

## Install apps using flatpak

```bash
bash install-flatpaks.sh
```

## Create toolbox container

```bash
toolbox create --image quay.io/toolbx-images/ubuntu-toolbox:22.04 main
```

## Configure the toolbox container

```bash
toolbox enter main
cd ~/mega/devops/distrobox/pybox
bash init.sh
```

## Install vs code extensions

```bash
toolbox enter main
cd ~/mega/devops/distrobox/pybox
bash install-vscode-extensions.sh
```

## Optional: Save and load distrobox container if needed

Following method is untested by me so far.

You can replace `podman` with `docker` in the below commands if that's installed instead.
You can replace `toolbox` with `distrobox` in the below commands if that's installed instead.

### Save distrobox container

```bash
podman container commit -p main main:0.1
podman save main:0.1 | gzip > main_container.tar.gz
```

### Copy the above file to a different machine and run the following command:

```bash
podman load < main_container.tar.gz
```

### To create a new distrobox container from the above image, run the following command:

```bash
toolbox create --image main:0.1 --name main
toolbox enter --name main
```

